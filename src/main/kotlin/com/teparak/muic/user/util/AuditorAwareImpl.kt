package com.teparak.muic.user.util

import com.teparak.muic.user.security.SecureUserDTO
import org.springframework.data.domain.AuditorAware
import org.springframework.security.core.context.SecurityContextHolder
import java.util.*

class AuditorAwareImpl : AuditorAware<SecureUserDTO> {
    override fun getCurrentAuditor(): Optional<SecureUserDTO> {
        val principal = SecurityContextHolder.getContext().authentication.principal
        return if  (principal is SecureUserDTO) Optional.of(principal) else Optional.ofNullable(null)
    }
}