package com.teparak.muic.user.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.context.annotation.Bean


fun simpleMapper(): ObjectMapper {
    return ObjectMapper().findAndRegisterModules().registerKotlinModule()
}