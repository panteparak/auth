package com.teparak.muic.user.util

import com.teparak.muic.user.security.SecureUserDTO
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
class AbstractAuditorDateTimeEntity : AbstractDateTimeEntity() {

    @CreatedBy
    private lateinit var createdBy: SecureUserDTO

    @LastModifiedBy
    private lateinit var modifiedBy: SecureUserDTO
}