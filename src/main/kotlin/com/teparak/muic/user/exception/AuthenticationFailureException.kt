package com.teparak.muic.user.exception

import org.springframework.security.core.AuthenticationException as AuthenticationException

class AuthenticationFailureException(msg: String) : AuthenticationException(msg)