package com.teparak.muic.user.exception

import java.lang.RuntimeException

class DataNotFoundException(message: String?) : RuntimeException(message)