package com.teparak.muic.user.exception

import java.lang.RuntimeException

class DataFoundException(message: String?) : RuntimeException(message)