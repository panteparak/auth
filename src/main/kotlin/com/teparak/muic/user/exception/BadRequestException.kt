package com.teparak.muic.user.exception

import java.lang.RuntimeException

class BadRequestException(message: String?) : RuntimeException(message)