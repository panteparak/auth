package com.teparak.muic.user.security.jwt

import com.teparak.muic.user.exception.AuthenticationFailureException
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.ServletException
import java.io.IOException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.FilterChain



class JWTAuthorizationFilter : UsernamePasswordAuthenticationFilter {

    constructor(pattern: String, authenticationManager: AuthenticationManager, successHandler: SimpleUrlAuthenticationSuccessHandler, failureHandler: SimpleUrlAuthenticationFailureHandler): super() {
        setAuthenticationSuccessHandler(successHandler)
        setAuthenticationFailureHandler(failureHandler)
        setAuthenticationManager(authenticationManager)
        setFilterProcessesUrl(pattern)
    }

    @Throws(AuthenticationException::class)
    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val token = request.getHeader("Authorization") ?: throw AuthenticationFailureException("Invalid 'Authorization' Token")
        return this.authenticationManager.authenticate(JWTAuthorizationToken(token.removePrefix("Bearer ")))
    }

    @Throws(IOException::class, ServletException::class)
    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        val context = SecurityContextHolder.createEmptyContext()!!
        context.authentication = authResult
        SecurityContextHolder.setContext(context)
        chain.doFilter(request, response)
    }
}


data class JWTAuthorizationToken(val token: String, val roles: Collection<out GrantedAuthority>?) :  AbstractAuthenticationToken(roles) {

    constructor(token: String) : this(token, null)

    override fun getCredentials(): String {
        return token
    }

    override fun getPrincipal(): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}