package com.teparak.muic.user.security.usercredential

import com.teparak.muic.user.exception.AuthenticationFailureException
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.util.ArrayList

@Component
class UserCredentialAuthorizationProvider : AuthenticationProvider {
    override fun authenticate(authentication: Authentication): Authentication {
        if (authentication !is UsernamePasswordAuthenticationToken) {
            throw AuthenticationFailureException("Invalid Credentials")
        }

        val username = authentication.name!!
        val password = authentication.credentials as String

        if (!username.contentEquals("admin") || !password.contentEquals("password")) {
            throw AuthenticationFailureException("Invalid Credentials")
        }

        return UsernamePasswordAuthenticationToken(username, null, ArrayList<GrantedAuthority>())
    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication.isAssignableFrom(UserCredentialAuthorizationToken::class.java)
    }

}