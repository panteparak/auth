package com.teparak.muic.user.security

import org.springframework.security.core.GrantedAuthority
import java.lang.Exception
import java.time.Instant
import javax.servlet.http.HttpServletResponse

data class LoginResponse(val message: String?, val status: Int, val authenticated: Boolean, val role: List<GrantedAuthority> = ArrayList(), val timestamp: Instant =  Instant.now()) {

    constructor() : this("", HttpServletResponse.SC_OK, true)
    constructor(e: Exception) : this(e.message!!, HttpServletResponse.SC_UNAUTHORIZED, false)
}