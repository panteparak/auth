package com.teparak.muic.user.security.jwt

import com.teparak.muic.user.exception.AuthenticationFailureException
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class JWTAuthenticationProvider : AuthenticationProvider {

    override fun authenticate(authentication: Authentication): Authentication {
        TODO("Implement Me!")
        if (authentication is JWTAuthorizationToken && authentication.token.startsWith("abc", true)) {

            return authentication
        } else {
            throw AuthenticationFailureException("Invalid Token")
        }
    }

    override fun supports(authentication: Class<*>?): Boolean {
        return authentication!!.isAssignableFrom(JWTAuthorizationToken::class.java)
    }

}