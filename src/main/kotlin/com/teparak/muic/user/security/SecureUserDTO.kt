package com.teparak.muic.user.security

import com.teparak.muic.user.userdetails.UserAccount

data class SecureUserDTO(val user: UserAccount){
    val username = user.username
}