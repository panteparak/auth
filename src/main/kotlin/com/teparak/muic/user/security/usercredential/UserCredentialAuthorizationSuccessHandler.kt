package com.teparak.muic.user.security.usercredential

import com.fasterxml.jackson.databind.ObjectMapper
import com.teparak.muic.user.security.LoginResponse
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class UserCredentialAuthorizationSuccessHandler : SimpleUrlAuthenticationSuccessHandler() {

    @Qualifier("objectMapper")
    @Autowired
    private lateinit var mapper: ObjectMapper

    override fun onAuthenticationSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {
        val loginResponse = LoginResponse()
        response.status = loginResponse.status
        response.contentType = MediaType.APPLICATION_JSON_VALUE
        clearAuthenticationAttributes(request)
        IOUtils.write(mapper.writeValueAsString(loginResponse), response.writer)
    }
}