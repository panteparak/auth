package com.teparak.muic.user.security

import com.teparak.muic.user.security.jwt.*
import com.teparak.muic.user.security.usercredential.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler
import javax.servlet.http.HttpServletRequest

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityAdapter {


    @EnableWebSecurity
    @Order(10)
    class UserCredentialWebSecurityAdapter : WebSecurityConfigurerAdapter() {

        val LOGIN_PATH = "/login"

        @Autowired
        lateinit var success: UserCredentialAuthorizationSuccessHandler

        @Autowired
        lateinit var failure: RESTAuthorizationFailureHandler

        @Autowired
        lateinit var entry: RESTAuthorizationEntryHandler

        @Autowired
        lateinit var authProvider: UserCredentialAuthorizationProvider

        override fun configure(auth: AuthenticationManagerBuilder) {
            auth.authenticationProvider(authProvider)
        }

        override fun configure(web: WebSecurity) {
            web
                    .ignoring()
                    .antMatchers("/actuator/health")
        }


        override fun configure(http: HttpSecurity) {
            http.csrf().disable()

            http.requestMatcher(object : AntPathRequestMatcherWrapper(){
                override fun precondition(request: HttpServletRequest): Boolean {

                    val matchLoginPath = request.servletPath.equals(LOGIN_PATH, ignoreCase = true)
                    val hasAuthorizeTypeHeader = request.getHeader("Authorization-Type") == null
                    val hasAuthorizaToken = request.getHeader("Authorization") == null
                    return matchLoginPath || hasAuthorizeTypeHeader || hasAuthorizaToken
                }
            })
            http
                    .exceptionHandling()
                    .authenticationEntryPoint(entry)
            http
                    .addFilterBefore(
                            UsernamePasswordExtractionFilter(
                                    pattern = LOGIN_PATH,
                                    authenticationManager = authenticationManager(),
                                    successHandler = success,
                                    failureHandler = failure), UsernamePasswordAuthenticationFilter::class.java
                    )
                    .formLogin()

            http
                    .logout().permitAll()
                    .clearAuthentication(true)
                    .invalidateHttpSession(true).deleteCookies("JSESSIONID", "AUTOLOGIN")
                    .logoutSuccessHandler(HttpStatusReturningLogoutSuccessHandler())

            http.authorizeRequests()
                    .anyRequest()
                    .authenticated()
        }
    }

    @EnableWebSecurity
    @Order(1)
    class JWTWebSecurityAdapter : WebSecurityConfigurerAdapter() {

        @Autowired
        lateinit var success: JWTAuthorizationSuccessHandler

        @Autowired
        lateinit var failure: RESTAuthorizationFailureHandler

        @Autowired
        lateinit var entry: RESTAuthorizationEntryHandler

        @Autowired
        lateinit var authProvider: JWTAuthenticationProvider

        @Autowired
        lateinit var userAuthProvider: UserCredentialAuthorizationProvider

        val LOGIN_PATH = "/login/jwt"

        override fun configure(http: HttpSecurity) {
            http.csrf().disable()

            http.requestMatcher(object : AntPathRequestMatcherWrapper() {
                override fun precondition(request: HttpServletRequest): Boolean {
                    val matchLoginPath = request.servletPath.equals(LOGIN_PATH, ignoreCase = true)
                    val jwtAuthHeader = request.getHeader("Authorization-Type").equals("jwt", ignoreCase = true)
                    val noAuthHeader = request.getHeader("Authorization-Type") == null
                    val hasAuthToken = request.getHeader("Authorization") != null
                    return matchLoginPath or (jwtAuthHeader or noAuthHeader) and hasAuthToken
                }
            })

            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            http.exceptionHandling().authenticationEntryPoint(entry)
            http
                    .addFilter(
                            UsernamePasswordExtractionFilter(
                                    pattern = "/login/jwt",
                                    authenticationManager = authenticationManager(),
                                    successHandler = success,
                                    failureHandler = failure)
                    )

            http.addFilter(
                    JWTAuthorizationFilter(
                            pattern = "/**",
                            authenticationManager = authenticationManager(),
                            successHandler = success,
                            failureHandler = failure
                    )
            )
        }


        override fun configure(auth: AuthenticationManagerBuilder) {
            auth.authenticationProvider(userAuthProvider)
            auth.authenticationProvider(authProvider)
        }
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }
}
