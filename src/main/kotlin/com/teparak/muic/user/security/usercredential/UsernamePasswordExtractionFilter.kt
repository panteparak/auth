package com.teparak.muic.user.security.usercredential

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.stereotype.Component
import java.io.InputStream
import java.nio.charset.Charset
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class UsernamePasswordExtractionFilter : UsernamePasswordAuthenticationFilter {

    constructor(pattern: String, authenticationManager: AuthenticationManager, successHandler: SimpleUrlAuthenticationSuccessHandler, failureHandler: SimpleUrlAuthenticationFailureHandler): super() {
        setAuthenticationSuccessHandler(successHandler)
        setAuthenticationFailureHandler(failureHandler)
        setAuthenticationManager(authenticationManager)
        setFilterProcessesUrl(pattern)
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication? {

        if (request.method != "POST"){
            throw AuthenticationServiceException("Authentication method not supported: '${request.method}'")
        }

        val parsedUserDetails = obtainUsernamePassword(request.inputStream)
        return this.authenticationManager.authenticate(parsedUserDetails)
    }

    private fun obtainUsernamePassword(inputStream: InputStream): UserCredentialAuthorizationToken {
        val payload = IOUtils.toString(inputStream, Charset.defaultCharset())
        val parsed = ObjectMapper().
                configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerKotlinModule()
                .readValue<UserCredentialAuthorizationToken>(payload)
        return parsed
    }
}

data class UserCredentialAuthorizationToken(val username: String, val password: String) : UsernamePasswordAuthenticationToken(username, password)