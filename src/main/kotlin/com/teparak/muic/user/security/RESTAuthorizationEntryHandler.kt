package com.teparak.muic.user.security

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RESTAuthorizationEntryHandler : AuthenticationEntryPoint {

    @Qualifier("objectMapper")
    @Autowired
    lateinit var mapper: ObjectMapper

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, ex: AuthenticationException) {
        val loginResponse = LoginResponse(ex)
        response.status = loginResponse.status
        response.contentType = MediaType.APPLICATION_JSON_VALUE
        IOUtils.write(mapper.writeValueAsString(loginResponse), response.writer)
        IOUtils.closeQuietly(response.writer)
    }
}

