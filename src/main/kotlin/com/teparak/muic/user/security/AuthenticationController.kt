package com.teparak.muic.user.security

import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant


@RestController
@RequestMapping("/")
class AuthenticationController {

    @GetMapping("/whoami")
    fun whoami(auth: Authentication) {

        val principal = auth.principal
        if (principal is SecureUserDTO){
            println("Principal")
        }
    }
}