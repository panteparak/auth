package com.teparak.muic.user.security

import com.fasterxml.jackson.databind.ObjectMapper
import com.teparak.muic.user.security.LoginResponse
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RESTAuthorizationFailureHandler : SimpleUrlAuthenticationFailureHandler() {

    @Qualifier("objectMapper")
    @Autowired
    private lateinit var mapper: ObjectMapper

    override fun onAuthenticationFailure(request: HttpServletRequest, response: HttpServletResponse, exception: AuthenticationException) {
        val loginResponse = LoginResponse(exception)
        response.status = loginResponse.status
        response.contentType = MediaType.APPLICATION_JSON_VALUE
        IOUtils.write(mapper.writeValueAsString(loginResponse), response.writer)
    }
}