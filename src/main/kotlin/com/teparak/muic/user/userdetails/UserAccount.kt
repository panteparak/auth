package com.teparak.muic.user.userdetails

import com.teparak.muic.user.util.AbstractDateTimeEntity
import org.hibernate.annotations.ColumnDefault
import java.util.*
import javax.persistence.*

@Entity
class UserAccount private constructor(): AbstractDateTimeEntity() {
    @Column(unique = true, nullable = false)
    @GeneratedValue
    lateinit var uid: UUID

    @Column(unique = true, nullable = false)
    lateinit var username: String

    @Column(nullable = false)
    lateinit var password: String

    @Column(nullable = false)
    @ColumnDefault("false")
    var disabled: Boolean = false


    companion object {
        fun createUser(username: String, hashPassword: String): UserAccount {
            val user = UserAccount()
            user.username = username
            user.password = hashPassword
            return user
        }
    }
}