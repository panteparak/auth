package com.teparak.muic.user.userdetails

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserDAO : JpaRepository<UserAccount, Long> {
    fun findByUsername(username: String): Optional<UserAccount>
}