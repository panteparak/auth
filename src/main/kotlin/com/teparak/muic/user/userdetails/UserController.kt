package com.teparak.muic.user.userdetails

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/user", consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
class UserController(val userService: UserService) {

    @PostMapping
    fun createUser(@RequestBody createUserData: CreateUserData) {
        userService.createUser(createUserData)
    }

    @PatchMapping
    fun modifyUserDetails(@RequestBody modifiedUserDetails: ModifiedUserDetails) {
        println(modifiedUserDetails)
    }

    @DeleteMapping
    fun disableUser(){

    }
}

data class CreateUserData(val username: String, val password: String, val confirmPassword: String)
data class ModifiedUserDetails(val password: String? = null, val confirmPassword: String? = null)