package com.teparak.muic.user.userdetails

import com.teparak.muic.user.exception.BadRequestException
import com.teparak.muic.user.exception.DataNotFoundException
import com.teparak.muic.user.security.SecureUserDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl : UserService {

    @Autowired
    private lateinit var userDAO: UserDAO

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    override fun getUserAccount(user: SecureUserDTO): UserAccount {
        return userDAO
                .findByUsername(user.username)
                .orElseThrow { DataNotFoundException("Username") }
    }

    override fun createUser(user: CreateUserData): UserDTO {

        val (username, password, confirmPassword) = user
        if (password != confirmPassword){
            throw BadRequestException("Password mismatch")
        }

        var hashPassword = passwordEncoder.encode(password)
        var user = UserAccount.createUser(username, hashPassword)

        user = userDAO.save(user)
        return UserDTO(user)
    }

    override fun modifiedUser(user: ModifiedUserDetails): UserDTO {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

