package com.teparak.muic.user.userdetails

import com.teparak.muic.user.security.SecureUserDTO

interface UserService {
    fun getUserAccount(user: SecureUserDTO): UserAccount
    fun createUser(user: CreateUserData): UserDTO
    fun modifiedUser(user: ModifiedUserDetails): UserDTO
}