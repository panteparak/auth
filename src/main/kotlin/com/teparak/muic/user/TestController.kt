package com.teparak.muic.user

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
@RequestMapping("/")
class TestController {


    @GetMapping
    fun test() : Instant {
        return Instant.now()
    }
}