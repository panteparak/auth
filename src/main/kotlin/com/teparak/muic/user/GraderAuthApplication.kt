package com.teparak.muic.user

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
class GraderAuthApplication

fun main(args: Array<String>) {
    runApplication<GraderAuthApplication>(*args)
}

